﻿using System;
using System.Collections.Generic;
using Characters.MapWalkers;

namespace Map.Layout
{
    [Serializable]
    public class MapData
    {
        public Point size = new Point(0, 0);
        public Dictionary<Point, string> terrain = new Dictionary<Point, string>();
        //public Dictionary<Point, WalkController> characters = new Dictionary<Point, WalkController>();
    }
}