﻿using System;
using System.Text.RegularExpressions;

namespace Map
{
	[Serializable]
	public struct Point : IComparable<Point> {
		public readonly int x;
		public readonly int y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public Point add(Point other) {
			return new Point (x + other.x, y + other.y);
		}

		public int CompareTo (Point other) {
			return x == other.x ? y.CompareTo (other.y) : x.CompareTo (other.x);
		}

		public override bool Equals (object obj) {
			if (obj == null || GetType () != obj.GetType ()) {
				return false;
			}
			return (this == (Point)obj);
		}

		public override string ToString () {
			return "(" + x + ", " + y + ")";
		}

		public override int GetHashCode()
		{
			return x ^ (7 * y);
		}

		public static bool operator ==(Point a, Point b) {
			if (ReferenceEquals(a, b)) {
				return true;
			}

			if ((object) a == null || (object) b == null) {
				return false;
			}

			return a.x == b.x && a.y == b.y;
		}

		public static bool operator !=(Point a, Point b) {
			return !(a == b);
		}

		public static Point operator +(Point a, Point b) {
			return new Point (a.x + b.x, a.y + b.y);
		}

		public static Point operator -(Point a, Point b) {
			return a + -b;
		}

		public static Point operator -(Point a) {
			return new Point(-a.x, -a.y);
		}
	}
}
