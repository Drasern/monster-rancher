﻿using System.Collections.Generic;

namespace Map
{
	public interface IMapGrid {

		GridPoint GetPoint (int x, int y);
		GridPoint GetPoint (Point coordinate);
		List<Point> GetAllPoints ();
		List<Point> GetNeighbours (GridPoint point);
		List<Point> GetNeighbours (Point coordinate);
		float Distance (Point a, Point b);
	}
}
