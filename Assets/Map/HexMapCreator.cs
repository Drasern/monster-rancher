﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Map
{
    public class HexMapCreator : HexMap
    {
        [SerializeField]
        public Point size = new Point(3, 3);
        private Point currentSize = new Point(0, 0);

        public string resourcePath;
	
        // Update is called once per frame
        void Update () {
//		Debug.Log (string.Format ("Hexmap [{0}]: Update()", gameObject.name));

	        if (map.Count == 0) {
		        Debug.Log (string.Format ("Hexmap [{0}]: Restoring map from existing objects", gameObject.name));
		        // We need to reinitalise the map from extant game objects

		        GridPoint[] current = GetComponentsInChildren<GridPoint> ();
		        foreach (GridPoint p in current) {
			        GridPoint existing;
			        if (map.TryGetValue (p.coord, out existing)) {
				        // we already have a point here
				        if (p != existing) {
					        // and it's a different obj
					        // just delete this new one
					        DestroyImmediate (p.gameObject);
				        }
			        } else {
				        // There wasn't one
				        map [p.coord] = p;
				        //Debug.Log ("Initalising with coordinate " + p.coord);
			        }
		        }
	        }

	        // if (gridPrefab != null && (size != currentSize)) {

		        List<Point> newCoords = GenerateSquare (size.x, size.y);

		        if ((currentSize.x > size.x || currentSize.y > size.y)) {
			        Debug.Log (string.Format ("Hexmap [{0}]: Contracting map ({1}->{2}, {3}->{4})", gameObject.name, currentSize.x, size.x, currentSize.y, size.y));

			        // Delete existing excess nodes
			        Stack<GridPoint> existing = new Stack<GridPoint> (map.Values);

			        while (existing.Count > 0) {
				        GridPoint p = existing.Pop ();
				        if (!newCoords.Contains (p.coord)) {
					        //Debug.Log ("Removing coordinate " + p.coord);
					        map.Remove (p.coord);
					        DestroyImmediate (p.gameObject);
				        }
			        }
		        } else {
			        Debug.Log (string.Format ("Hexmap [{0}]: Expanding map ({1}->{2}, {3}->{4})", gameObject.name, currentSize.x, size.x, currentSize.y, size.y));

			        // Add any missing nodes
			        foreach (Point c in newCoords) {
				        if (!map.ContainsKey (c)) {
					        GridPoint p = CreateAtPoint (c, 0);
					        map[c] = p;
				        }
			        }
		        }

		        currentSize = size;
		        // }
        }

		public void SaveToFile() {
			if (string.IsNullOrEmpty(resourcePath)) {
				resourcePath = Path.GetTempFileName ();
			}

			int i = 0;
			using (StreamWriter file = new StreamWriter (resourcePath, false)) {
				// First entry map size as point
				file.WriteLine (size.ToString ());
				i++;

				string terrain = "";
				// Second entry comma separated terrain types
				foreach (TerrainType t in terrainTypes) {
					terrain += "," + t.terrainCode;
				}
				file.WriteLine (terrain.Substring(1));

				List<GridPoint> list = new List<GridPoint> (map.Values);
				list.Sort ();
				foreach (GridPoint point in list) {
					file.WriteLine (point.ToString ());
					i++;
				}
				file.Flush ();
				file.Close (); 
			}

			Debug.Log ("Wrote " + i + " lines to file " + resourcePath);
		}

		public void LoadFromFile() {
			if (string.IsNullOrEmpty(resourcePath)) {
				return;
			}

			try {
				Point mapSize;
				int i = 0;
				Dictionary<Point, GridPoint> tmp = new Dictionary<Point, GridPoint>();

				// using (StreamReader file = new StreamReader (resourcePath)) {
				// 	// First entry map size as point
				// 	string sizeStr = file.ReadLine ();
				// 	mapSize = Point.Parse (sizeStr);
				// 	i++;
				//
				// 	// Second entry comma separated terrain types
				// 	//-- Load all list of all types and drop into a map
				// 	TerrainType[] alltypes = Resources.LoadAll<TerrainType>("Map/Materials");
				// 	Dictionary<string, TerrainType> typeMap = new Dictionary<string, TerrainType>();
				// 	foreach (TerrainType type in alltypes) {
				// 		typeMap.Add(type.terrainCode, type);
				// 	}
				//
				// 	//-- Read saved list and update local
				// 	string tts = file.ReadLine ();
				// 	string[] typeCodes = tts.Split (',');
				// 	terrainTypes = new TerrainType[typeCodes.Length];
				// 	for (int index = 0; index < typeCodes.Length; index++) {
				// 		terrainTypes[index] = typeMap[typeCodes[index]];
				// 	}
				// 	i++;
				//
				// 	// other lines are GridPoint
				// 	string line = "";
				// 	while ((line = file.ReadLine ()) != null) {
				// 		Match m = GridPoint.parser.Match (line);
				//
				// 		if (m.Success) {
				// 			GridPoint point;
				// 			Point coord = Point.Parse (m.Groups [1].Value);
				// 			int elevation = Int32.Parse (m.Groups [2].Value);
				// 			string terrainCode = m.Groups[3].Value;
				//
				// 			if (!map.TryGetValue(coord, out point)) {
				// 				point = CreateAtPoint (coord, elevation);
				// 			}
				// 			point.terrain = typeMap[terrainCode];
				//
				// 			tmp.Add (coord, point);
				// 			i++;
				// 		} else {
				// 			throw new FormatException (string.Format ("Unable to parse string \"{0}\" into GridPoint", line)); 
				// 		}
				// 	}
				// }

				// Successfull parse, update main variables
				Debug.Log ("Read " + i + " lines from file " + resourcePath);
				// size = currentSize = mapSize;
				map = tmp;
			} catch (System.Exception ex) {
				throw new FileLoadException ("Failed to parse file at " + resourcePath, ex);
			}
		}
    }
}