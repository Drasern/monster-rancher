﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Map
{
	[System.Serializable]
	public class GridPoint : MonoBehaviour, IComparable<GridPoint> {
		public Point coord;
		public int elavation = 0;
		public TerrainType terrain;

		public static Regex parser = new Regex(@"\[(\(-?\d+, -?\d+\)), (-?\d+), (\w*)\]");

		// Use this for initialization
		void Start () {
		}

		// Update is called once per frame
		void Update () {
			Vector3 p = transform.position;
			if (Mathf.RoundToInt (p.y) != elavation) {
				p.y = elavation;
				transform.position = p;
			}
		}

		public int CompareTo (GridPoint other) {
			if (other == null) {
				return 1;
			}

			return coord.CompareTo (other.coord);
		}

		public override string ToString () {
			return string.Format ("[{0}, {1}, {2}]", coord.ToString (), elavation, terrain != null ? terrain.terrainCode : "");
		}
	}
}
