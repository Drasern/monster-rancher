﻿using System;
using UnityEngine;

namespace Map
{
	[CreateAssetMenu(menuName = "TerrainType")]
	[Serializable]
	public class TerrainType : ScriptableObject {
		public string terrainCode;
		[NonSerialized]
		public GridPoint prefab;
	}
}
