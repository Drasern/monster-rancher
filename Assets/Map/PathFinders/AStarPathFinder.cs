﻿using System.Collections.Generic;
using UnityEngine;

namespace Map.PathFinders
{
	public class AStarPathFinder : IPathFinder {
		public delegate float Heuristic(Point a, Point b);
		public delegate float Cost(Point a, Point b);

		private IMapGrid map;
		private Heuristic h;
		private Cost c;

		private struct Node {
			public float gCost;	// Actual cost to node
			public float fCost;	// Heuristic cost from node to goal
			public Point prev;

			public Node(float g, float f, Point p) {
				gCost = g;
				fCost = f;
				prev = p;
			}
		}

		public AStarPathFinder(IMapGrid mapImpl, Cost cfunc) {
			map = mapImpl;
			h = map.Distance;
			c = cfunc;
		}

		public List<Point> FindPath (Point src, Point dst) {

			// --- Search the space
			Dictionary<Point, Node> openNodes = new Dictionary<Point, Node> ();
			Dictionary<Point, Node> closedNodes = new Dictionary<Point, Node> ();

			Point point = src;
			Node node = new Node (0, h(src, dst), src);

			openNodes [point] = node;
			while (openNodes.Count > 0 && point != dst) {
				// Expand node
				openNodes.Remove (point);
				closedNodes [point] = node;

				// Update neighbour costs
				foreach (Point nPoint in map.GetNeighbours (point)) {
					if (!closedNodes.ContainsKey (nPoint)) {
						// We have an in progress node
						Node nNode;
						float cost = node.gCost + c(point, nPoint);

						if (openNodes.TryGetValue (nPoint, out nNode)) {
							// It's already been encountered
							if (cost < nNode.gCost) {
								// But the new path is shorter
								nNode.gCost = cost;
								nNode.prev = point;
							}
						} else {
							// It's a brand new node
							openNodes [nPoint] = new Node(cost, h(nPoint, dst), point);
						}
					}
				}

				// Select next node
				float candidateCost = float.MaxValue;
				foreach (Point nPoint in openNodes.Keys) {
					Node nNode = openNodes [nPoint];

					if (nNode.gCost + nNode.fCost < candidateCost) {
						// We have a new best candidate
						point = nPoint;
						node = nNode;
						candidateCost = nNode.gCost + nNode.fCost;
					}
				}

				// If no new candidate is found, openNodes must be empty and we break the loop
			}

			// --- Reconstruct the path

			if (point != dst) {
				// No path was found
				// Return the path that gets us heuristically closest
				float candidateCost = float.MaxValue;
				foreach (Point nPoint in closedNodes.Keys) {
					Node nNode = closedNodes [nPoint];

					if (nNode.gCost + nNode.fCost < candidateCost) {
						// We have a new best candidate
						point = nPoint;
						node = nNode;
						candidateCost = nNode.gCost + nNode.fCost;
					}
				}
			}
			closedNodes [point] = node;


			Stack<Point> stack = new Stack<Point> ();

			while (point != null) {
				stack.Push (point);
				node = closedNodes [point];
				point = node.prev;
			}

			List<Point> list = new List<Point> (stack);


			string str = "";

			foreach (Point coord in list) {
				str += ", " + coord;
			}
			if (str.Length > 0) { 
				str = str.Substring (2);
			}
			Debug.Log("Found path [" + str + "]");

			return list;
		}
	}
}
