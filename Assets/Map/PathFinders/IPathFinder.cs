﻿using System.Collections.Generic;

namespace Map.PathFinders
{
	public interface IPathFinder {
		List<Point> FindPath(Point src, Point dst);
	}
}
