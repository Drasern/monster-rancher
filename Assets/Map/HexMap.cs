﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Map.Layout;
using UnityEditor;
using UnityEngine;

namespace Map
{
	[ExecuteInEditMode]
	public class HexMap : MonoBehaviour, IMapGrid {
		// Tile sizing constants
		protected static readonly  float HorizontalSpacing = 3f/4f;
		protected static readonly  float VerticalSpacing = Mathf.Sqrt(3)/2;
		protected static readonly float HexWidth = 1 / VerticalSpacing;

		public MapData data;
		public TerrainType[] terrainTypes;
		
		protected Dictionary<Point, GridPoint> map = new Dictionary<Point, GridPoint>();

		public GridPoint GetPoint(int x, int y) {
			return GetPoint (new Point (x, y));
		}

		public GridPoint GetPoint(Point c) {
			return map.TryGetValue(c, out var loc) ? loc : null;
		}

		public List<Point> GetAllPoints() {
			return new List<Point> (map.Keys);
		}

		public List<Point> GetNeighbours(GridPoint point) {
			return GetNeighbours (point.coord);
		}

		public List<Point> GetNeighbours(Point coord) {
			List<Point> grid = GenerateRadius (1);
			List<Point> neighbours = new List<Point> ();

			foreach (Point c in grid) {
				Point neighbour = coord + c;
				if (map.ContainsKey (neighbour)) {
					neighbours.Add (neighbour);
				}
			}

			return neighbours;
		}

		public float Distance (Point a, Point b) {
			Point diff = a - b;
			// Hexagonal manhattan distance
			return Mathf.Max (Mathf.Abs (diff.x), Mathf.Abs (diff.y), Mathf.Abs (diff.x + diff.y));
		}

		protected List<Point> GenerateSquare(int width, int height) {
			List<Point> coordsList = new List<Point>(); 

			for (int x = -width; x <= width; x++) {
				int start = -height - Mathf.FloorToInt (x / 2f);
				int end = height - Mathf.CeilToInt (x / 2f);

				for (int y = start; y <= end; y++) {
					coordsList.Add (new Point (x, y));
				}
			}
			return coordsList;
		}

		protected List<Point> GenerateRadius(int radius) {
			List<Point> coordsList = new List<Point>(); 

			for (int x = -radius; x <= radius; x++) {
				int start = Mathf.Max(-radius, -x - radius);
				int end   = Mathf.Min(radius, -x + radius);

				for (int y = start; y <= end; y++) {
					coordsList.Add (new Point (x, y));
				}
			}
			return coordsList;
		}

		protected GridPoint CreateAtPoint(Point coord, int elevation, int terrain = 0) {
			GridPoint p = (GridPoint) PrefabUtility.InstantiatePrefab (terrainTypes[terrain].prefab);
			p.transform.parent = transform;
			p.coord = coord;
			p.name = string.Format ("Coordinate " + p.coord);
			p.transform.localPosition = PositionFromCoord (p.coord) + Vector3.up * elevation;

			return p;
		}
		
		protected Vector3 PositionFromCoord(Point c) {
			float x = c.x * HexWidth * HorizontalSpacing;
			float y = (c.x / 2f + c.y) * HexWidth * VerticalSpacing;
			return new Vector3 (x, 0, y);
		}
	}
}
