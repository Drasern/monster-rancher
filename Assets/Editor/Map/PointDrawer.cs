﻿using System.Collections;
using System.Collections.Generic;
using Map;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Point))]
public class PointDrawer : PropertyDrawer {
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.BeginProperty (position, label, property);
		EditorGUI.indentLevel = 0;
		Rect content = EditorGUI.PrefixLabel (position, label);

		EditorGUIUtility.labelWidth = 12f;

		content.width = (content.width / 2) -2;
		EditorGUI.DelayedIntField (content, property.FindPropertyRelative ("x"));

		content.x += content.width + 4;
		EditorGUI.DelayedIntField (content, property.FindPropertyRelative ("y"));

		EditorGUI.EndProperty ();
	}
}
