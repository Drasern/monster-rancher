﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(GridPoint))]
[CanEditMultipleObjects]
public class GridPointEditor : Editor {
	SerializedProperty coord;
	SerializedProperty elevation;
	SerializedProperty terrain;

	void OnEnable() {
		coord 		= serializedObject.FindProperty ("coord");
		elevation 	= serializedObject.FindProperty ("elevation");
		terrain 	= serializedObject.FindProperty ("terrain");
	}

	public override void OnInspectorGUI () {
		serializedObject.Update ();

		EditorGUILayout.DelayedTextField (elevation);
		EditorGUILayout.PropertyField (terrain);

		serializedObject.ApplyModifiedProperties ();
	}
}
