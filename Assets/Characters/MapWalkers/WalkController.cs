﻿using System;
using System.Collections.Generic;
using Map;
using UnityEngine;

namespace Characters.MapWalkers
{
	public abstract class WalkController : MonoBehaviour {
		protected enum MoveState {
			Moving,
			Waiting
		}

		[NonSerialized]
		protected MoveState moveState = MoveState.Waiting;

		public HexMap map;
		public Point startingPoint = new Point (0, 0);
		[NonSerialized]
		public GridPoint location;

		[NonSerialized]
		public GridPoint destination;
		[NonSerialized]
		public List<Point> path = new List<Point> ();

		public float walkSpeed = 1.0f;
		public Vector3 offset = Vector3.up;

		public float nodeWaitTime = 0.2f;
		protected float timer = 0;

		// Use this for initialization
		virtual public void Start () {
			Debug.Log (string.Format ("WalkController [{0}]: Start()", gameObject.name));

			if (map == null) {
				map = FindObjectOfType<HexMap> ();
			}

			if (location == null) {
				location = map.GetPoint (startingPoint);
			}

			transform.position = location.transform.position + offset;
		}

		virtual public void Update() {
			timer += Time.deltaTime;
			if (timer >= nodeWaitTime && moveState == MoveState.Waiting) {
				NextDestination ();
			}
		}

		void FixedUpdate () {
			if (destination == null) {
				return;
			}

			float movement = walkSpeed * Time.deltaTime;
			Vector3 direction = destination.transform.position - transform.position + offset;

			if (movement < direction.magnitude) {
				// Not going to overshoot, move in direction
				transform.position += movement * direction.normalized;
			} else {
				// Going to overshoot, move to destination
				transform.position = destination.transform.position + offset;

				OnArrival (destination);
				destination = null;
			}
		}

		void OnDrawGizmos() {
			if (destination != null) {
				Gizmos.color = Color.blue;
				Gizmos.DrawLine (transform.position, destination.transform.position + offset);
			}
		}

		virtual protected void OnArrival(GridPoint p) {
			Debug.Log (string.Format ("WalkController [{0}]: OnArrival({1})", gameObject.name, p.coord));
			location = p;

			moveState = MoveState.Waiting;
			timer = 0;
		}
		
		protected void NextDestination () {
			if (path.Count == 0) {
				// We don't have a destination, we're just going to wait.
				return;
			}
			Debug.Log (string.Format ("WalkController [{0}]: NextDestination()", gameObject.name));

			if (moveState == MoveState.Waiting) {
				moveState = MoveState.Moving;
			}

			// Move to next node in path
			destination = map.GetPoint (path [0]);
			path.RemoveAt (0);
		}
	}
}
