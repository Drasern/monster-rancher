﻿using Map;
using Map.PathFinders;
using UnityEngine;

namespace Characters.MapWalkers
{
	public class MouseClickWalkController : WalkController {
		public Camera cam;

		private IPathFinder pathFinder;

		// Use this for initialization
		override public void Start () {
			base.Start ();

			if (cam == null) {
				cam = Camera.main;
			}
			if (map == null) {
				map = FindObjectOfType<HexMap> ();
			}

			pathFinder = new AStarPathFinder(map, Cost);
		}
	
		// Update is called once per frame
		override public void Update () {
			base.Update ();

			if (Input.GetButtonDown ("Fire1")) {
				Ray ray = cam.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast (ray, out hit)) {
					GameObject obj = hit.collider.gameObject;

					Debug.Log ("Clicked on " + obj.name);

					GridPoint target = obj.GetComponent<GridPoint> ();
					if (target != null) {
						GridPoint start;
						if (destination != null) {
							// If we're moving somewhere, start the path from there
							start = destination;
						} else {
							// Else start it from where we currently are
							start = location;
						}

						path = pathFinder.FindPath (start.coord, target.coord);
						if (path.Count > 1 && path [0] == start.coord) {
							path.RemoveAt (0);
						}
					}
				}
			}
		}
		
		private float Cost(Point a, Point b) {
			return map.Distance (a, b);
		}
	}
}
