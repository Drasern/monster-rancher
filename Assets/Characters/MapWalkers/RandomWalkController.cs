﻿using System.Collections.Generic;
using Map;
using UnityEngine;

namespace Characters.MapWalkers
{
	public class RandomWalkController : WalkController {

		// Update is called once per frame
		void Update () {
//		if (destination != null) {
//			if (moveState == MoveState.Waiting) {
//				if (path.Count == 0) {
//					// We're at final destination of our path
//					if (timer >= ArrivalWaitTime) {
//						NextDestination ();
//						moveState = MoveState.Moving;
//					}
//
//				} else if (timer >= nodeWaitTime) {
//					// We're at an intermediate destination
//					NextDestination ();
//					moveState = MoveState.Moving;
//				}
//				timer += Time.deltaTime;
//			}
//		}
		}

		protected GridPoint GenerateDestination() {
			List<Point> candidates = map.GetAllPoints ();
			candidates.Remove (location.coord);		// don't want to just walk to our current point.
			int randInt = Random.Range (0, candidates.Count -1);

			return map.GetPoint(candidates[randInt]);
		}
	}
}
