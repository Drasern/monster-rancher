﻿using Characters.MapWalkers;
using UnityEngine;

namespace Characters
{
	[RequireComponent(typeof(PlayerWalkController))]
	public class Character : MonoBehaviour {

		public float health;
		public float strenght;
		public float speed;
		public string speciesName;

		public PlayerWalkController controller;

		// Use this for initialization
		void Start () {

			if (controller == null) {
				controller = GetComponent<PlayerWalkController> ();
			}
		}
	
		// Update is called once per frame
		void Update () {
		
		}
	}
}
