﻿using System.Collections.Generic;
using Map;
using Map.PathFinders;
using UnityEngine;

namespace Characters
{
	public class Party : MonoBehaviour {
		public string playerName;
		public Character[] party;
		public Character activeUnit;

		public Camera cam;
		public HexMap map;
		private IPathFinder pathFinder;

		// Use this for initialization
		public void Start () {
			if (cam == null) {
				cam = Camera.main;
			}
			if (map == null) {
				map = FindObjectOfType<HexMap> ();
			}

			pathFinder = new AStarPathFinder(map, Cost);
		}

		// Update is called once per frame
		public void Update () {
			if (Input.GetButtonDown ("Fire1")) {
				Ray ray = cam.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast (ray, out hit)) {
					GameObject obj = hit.collider.gameObject;

					Debug.Log ("Clicked on " + obj.name);

					GridPoint point;
					Character unit;
					if ( (point = obj.GetComponent<GridPoint> ()) != null) {
						bool wasSelect = false;
						foreach (Character c in party) {
							if (c.controller.location.coord == point.coord) {
								activeUnit = c;
								wasSelect = true;
								break;
							}
						}

						if (!wasSelect  && activeUnit != null) {
							GridPoint start = activeUnit.controller.location;
//						if (destination != null) {
//							// If we're moving somewhere, start the path from there
//							start = destination;
//						} else {
//							// Else start it from where we currently are
//							start = location;
//						}
//
							List<Point> path = pathFinder.FindPath (start.coord, point.coord);
							if (path.Count > 1 && path [0] == start.coord) {
								path.RemoveAt (0);
							}
							activeUnit.controller.path = path;
						}
					} else if ( (unit = obj.GetComponent<Character> ()) != null) {
						activeUnit = unit; 
					}
				}
			}
		}

		private float Cost(Point a, Point b) {
			List<Point> neighbours = map.GetNeighbours (a);
			if (!neighbours.Contains (b)) {
				return 999f;
			}

			GridPoint destination = map.GetPoint (b);

			if (destination.terrain == map.terrainTypes [1]) {
				return 5f;
			}

			return 1f;
		}
	}
}
